/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/UnitTests/JUnit4TestClass.java to edit this template
 */
package cat.xtec.ioc.service;

import cat.xtec.ioc.domain.Dentista;
import org.junit.Before;
import org.junit.Test;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.Invocation;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.GenericType;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriBuilder;
import java.net.URI;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;

/**
 *
 * @author jmorcuende
 * He aprés molt fent el mòdul 8 de DAW encara que hagi estat dur!!!
 */
public class DentistaJSONServiceTest {

    private static final Client client = ClientBuilder.newClient();
    private Dentista dentista1;
    private Dentista dentista2;

    @Before
    public void testBefore() {
        this.dentista1 = new Dentista("A1", 12.0d, 10.0d, false, LocalDateTime.parse("2023-10-10T10:10:10"));
        this.dentista2 = new Dentista("A2", 12.0d, 10.0d, false, LocalDateTime.parse("2023-10-10T10:10:10"));

        URI uri = UriBuilder.fromUri("http://localhost/dawm07eac5dentista/dentistajson/dentistes").port(8080).build();
        WebTarget target = client.target(uri);
        Invocation invocation1 = target.request(MediaType.APPLICATION_JSON).buildPost(Entity.entity(this.dentista1, MediaType.APPLICATION_JSON));
        Invocation invocation2 = target.request(MediaType.APPLICATION_JSON).buildPost(Entity.entity(this.dentista2, MediaType.APPLICATION_JSON));

        Response response1 = invocation1.invoke();
        assertEquals(Response.Status.NO_CONTENT.getStatusCode(), response1.getStatus());

        Response response2 = invocation2.invoke();
        assertEquals(Response.Status.NO_CONTENT.getStatusCode(), response2.getStatus());
    }

    @Test
    public void getAllDentistes() {
        URI uri = UriBuilder.fromUri("http://localhost/dawm07eac5dentista/dentistajson/dentistes").port(8080).build();
        WebTarget target = client.target(uri);
        Invocation invocation = target.request(MediaType.APPLICATION_JSON).buildGet();

        Response response = invocation.invoke();
        assertNotNull(response);
        List<Dentista> dentistes = response.readEntity(new GenericType<List<Dentista>>(){});

        List<String> codis = new ArrayList<>();
        for (Dentista dentista : dentistes) {
            codis.add(dentista.getCodi());
        }

        assertTrue(codis.contains(this.dentista1.getCodi()));
        assertTrue(codis.contains(this.dentista2.getCodi()));
    }

    @Test
    public void getDentistaByCodi() {
        URI uri = UriBuilder.fromUri("http://localhost/dawm07eac5dentista/dentistajson/dentistes/A1").port(8080).build();
        WebTarget target = client.target(uri);
        Invocation invocation = target.request(MediaType.APPLICATION_JSON).buildGet();

        Response response = invocation.invoke();
        assertNotNull(response);
        Dentista dentista = response.readEntity(Dentista.class);

        assertEquals(this.dentista1.getCodi(), dentista.getCodi());
    }

    @Test
    public void updatePacientDentista() {
        URI getDentistaUri = UriBuilder.fromUri("http://localhost/dawm07eac5dentista/dentistajson/dentistes/A1").port(8080).build();
        WebTarget getDentistaTarget = client.target(getDentistaUri);
        Invocation getDentistaInvocation = getDentistaTarget.request(MediaType.APPLICATION_JSON).buildGet();
        Response getDentistaRes = getDentistaInvocation.invoke();
        assertNotNull(getDentistaRes);
        Dentista dentista = getDentistaRes.readEntity(Dentista.class);

        URI updateDentistaUri = UriBuilder.fromUri("http://localhost/dawm07eac5dentista/dentistajson/dentistes/A1/true/10.0").port(8080).build();
        WebTarget updateDentistaTarget = client.target(updateDentistaUri);
        Invocation updateDentistaInvocation = updateDentistaTarget.request(MediaType.APPLICATION_JSON).buildPut(Entity.entity(dentista, MediaType.APPLICATION_JSON));
        Response updateDentistaRes = updateDentistaInvocation.invoke();
        assertNotNull(updateDentistaRes);
        Dentista dentistaUpdated = updateDentistaRes.readEntity(Dentista.class);

        assertEquals(dentistaUpdated.getPacientActual() , true);
    }
}

