package cat.xtec.ioc.service;

import cat.xtec.ioc.domain.Dentista;
import cat.xtec.ioc.repository.DentistaRepository;
import cat.xtec.ioc.repository.impl.InMemoryDentistaRepository;

import javax.ejb.Singleton;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import java.util.List;

@Path("/dentistes")
@Singleton
public class DentistaJSONService {

    private final DentistaRepository dentistaRepository = new InMemoryDentistaRepository();

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public List<Dentista> getAllDentistes() {
        return this.dentistaRepository.getAllDentistes();
    }

    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    public void addDentistaJSON(Dentista dentista) {
        this.dentistaRepository.addDentistaJSON(dentista);
    }

    @GET
    @Path("{codi}")
    @Produces(MediaType.APPLICATION_JSON)
    public Dentista getDentistaByCodi(@PathParam("codi") String codi) {
        return this.dentistaRepository.getDentistaByCodi(codi);
    }

    @PUT
    @Path("{codi}/{pacient}/{preuPeces}")
    @Produces(MediaType.APPLICATION_JSON)
    public Dentista updatePacientDentista(@PathParam("codi") String codi, @PathParam("pacient") Boolean pacient, @PathParam("preuPeces") Double preuPeces) {
        return this.dentistaRepository.updatePacientDentista(codi, pacient, preuPeces);
    }
}
