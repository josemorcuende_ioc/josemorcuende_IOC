package cat.xtec.ioc.service.impl;

import cat.xtec.ioc.domain.Dentista;
import cat.xtec.ioc.repository.DentistaRepository;
import cat.xtec.ioc.repository.impl.InMemoryDentistaRepository;
import cat.xtec.ioc.service.DentistaServiceEndpoint;

import javax.jws.WebService;
import java.util.List;

@WebService(serviceName = "DentistaService", endpointInterface = "cat.xtec.ioc.service.DentistaServiceEndpoint")
public class DentistaServiceEndpointImpl implements DentistaServiceEndpoint {

    private final DentistaRepository dentistaRepository = new InMemoryDentistaRepository();

    @Override
    public List<Dentista> initDentistes(String codisDentista) {
        return this.dentistaRepository.initDentistes(codisDentista);
    }

    @Override
    public List<Dentista> getAllDentistes() {
        return this.dentistaRepository.getAllDentistes();
    }

    @Override
    public Dentista getDentistaByCodi(String codi) {
        return this.dentistaRepository.getDentistaByCodi(codi);
    }

    @Override
    public Dentista updatePacientDentista(String codi, Boolean pacient, Double preuPeces) {
        return this.dentistaRepository.updatePacientDentista(codi, pacient, preuPeces);
    }
}
