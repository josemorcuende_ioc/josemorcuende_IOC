package cat.xtec.ioc.service;

import cat.xtec.ioc.domain.Dentista;

import javax.jws.WebMethod;
import javax.jws.WebService;
import javax.jws.soap.SOAPBinding;
import java.util.List;

@WebService
@SOAPBinding(style = SOAPBinding.Style.DOCUMENT)
public interface DentistaServiceEndpoint {
    @WebMethod List<Dentista> initDentistes(String codisDentista);
    @WebMethod List<Dentista> getAllDentistes();
    @WebMethod Dentista getDentistaByCodi(String codi);
    @WebMethod Dentista updatePacientDentista(String codi, Boolean pacient, Double preuPeces);
}
