package cat.xtec.ioc.repository;

import cat.xtec.ioc.domain.Dentista;

import java.util.List;

public interface DentistaRepository {
    List<Dentista> initDentistes(String codisDentista);
    void addDentistaJSON(Dentista dentista);
    List<Dentista> getAllDentistes();
    Dentista getDentistaByCodi(String codi);
    Dentista updatePacientDentista(String codi, Boolean pacient, Double preuPeces);
}
