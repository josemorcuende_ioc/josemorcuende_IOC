package cat.xtec.ioc.repository.impl;

import cat.xtec.ioc.domain.Dentista;
import cat.xtec.ioc.repository.DentistaRepository;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

public class InMemoryDentistaRepository implements DentistaRepository {

    private final List<Dentista> dentistes = new ArrayList<>();

    @Override
    public List<Dentista> initDentistes(String codisDentista) {
        String[] codis = codisDentista.split("##");
        for (String codi : codis) {
            Dentista dentista = new Dentista(
                codi,
                0.0d,
                0.0d,
                false,
                LocalDateTime.now()
            );
            this.addDentistaJSON(dentista);
        }

        return this.getAllDentistes();
    }

    @Override
    public void addDentistaJSON(Dentista dentista) {
        this.dentistes.add(dentista);
    }

    @Override
    public List<Dentista> getAllDentistes() {
        return this.dentistes;
    }

    @Override
    public Dentista getDentistaByCodi(String codi) {
        Dentista dentistaByCodi = null;
        for (Dentista d : this.dentistes) {
            if (d != null && d.getCodi() != null
                    && d.getCodi().equals(codi)) {
                dentistaByCodi = d;
                break;
            }
        }
        if (dentistaByCodi == null) {
            throw new IllegalArgumentException(
                    "No s'ha trobat el dentista amb el codi: " + codi);
        }
        return dentistaByCodi;
    }

    @Override
    public Dentista updatePacientDentista(String codi, Boolean pacient, Double preuPeces) {
        Dentista dentista = this.getDentistaByCodi(codi);

        if (pacient && preuPeces != null) {
            preuPeces += dentista.getPreuPeces();
            dentista.setPreuPeces(preuPeces);
        } else {
            Double segons = LocalDateTime.now().getSecond() * 1.0d - dentista.getDatePacient().getSecond() * 1.0d;
            segons += dentista.getPacientAcumulat();
            dentista.setPacientAcumulat(segons);
        }

        dentista.setPacientActual(pacient);

        return dentista;
    }
}
